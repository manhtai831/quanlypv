/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.*;
import controller.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.*;
import javax.swing.JButton;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Component;

public class view extends javax.swing.JFrame {

	/**
	 * Creates new form view
	 */
	public static final String FileFV = "PV.TXT", FileKB = "KB.TXT", FileDS = "DS.TXT";
	ArrayList<PhongVien> listPv;
	ArrayList<BaiViet> listBv;
	ArrayList<DanhSach> listDs;
	DefaultTableModel DefaultTableModelPV, DefaultTableModelBV, DefaultTableModelDS, DefaultTableModelTC;

	public view() {
		setTitle("Quản lý");
		initComponents();
		DefaultTableModelPV = (DefaultTableModel) tablePV.getModel();
		DefaultTableModelBV = (DefaultTableModel) tableKBV.getModel();
		DefaultTableModelDS = (DefaultTableModel) tableDS.getModel();
		DefaultTableModelTC = (DefaultTableModel) tableTc.getModel();
		listPv = new ArrayList<>();
		listBv = new ArrayList<>();
		listDs = new ArrayList<>();
		docfilevaotabel();
		napComb();

		// sử lí nút bấm thêm trong Phóng Viên
		themPVBTN.addActionListener((e) -> {
			try {
				String ten = hotenPVTF.getText();
				String dc = diachiPVTF.getText();
				String md = mucdoPVCB.getItemAt(mucdoPVCB.getSelectedIndex());
				int i = listPv.size();
				PhongVien a = new PhongVien(i, ten, dc, md);
				if (a.trungnhau(listPv)) {
					JOptionPane.showMessageDialog(null, "da bi trung");
					return;
				} else {
					listPv.add(a);
					DefaultTableModelPV.addRow(a.toObjects());
					napComb();
				}
			} catch (deTrongEX ex) {
				JOptionPane.showMessageDialog(null, "khong duoc bo trong");
			} catch (dangChuEX ex) {
				JOptionPane.showMessageDialog(null, "ho ten chua minh chu cai");
			}
		});
		// sử lí nút bấm thêm trong kiểu bài viết
		themKBVBTN.addActionListener((e) -> {
			String ten = tenKBVTF.getText();
			int gia = Integer.valueOf(dongiaKBVTF.getText());
			int i = listBv.size();
			try {
				BaiViet a = new BaiViet(i, ten, gia);
				if (a.trungnhau(listBv)) {
					JOptionPane.showMessageDialog(null, "da bi trung");
					return;
				} else {
					listBv.add(a);
					DefaultTableModelBV.addRow(a.toObjects());
					napComb();
				}
			} catch (dangSoEX ex) {
				JOptionPane.showMessageDialog(null, "don gia la gia tri nguyen duong");
			} catch (dangChuEX ex) {
				JOptionPane.showMessageDialog(null, "ho ten chua minh chu cai");
			} catch (deTrongEX ex) {
				JOptionPane.showMessageDialog(null, "khong duoc bo trong");
			}
		});
		// sử lí nút bấm thêm trong Danh sách
		themDSBTN.addActionListener((e) -> {
			try {
				int a = maphongvienDSCB.getSelectedIndex();
				int b = mabaivietDSCB.getSelectedIndex();
				String sl = soluongDSTF.getText();
				if (kiemTra.choSO(sl)) {
					JOptionPane.showMessageDialog(null, "so luong ko chua chu cai");
					return;
				}
				int soluong = Integer.valueOf(sl);
				DanhSach s = new DanhSach(listPv.get(a), listBv.get(b), soluong);
				s.setGia(listBv.get(b).getGia());
				s.setThanhtien();
				if (s.trungnhau(listDs)) {
					JOptionPane.showMessageDialog(null, "da bi trung");
					return;
				} else {
					listDs.add(s);
					DefaultTableModelDS.addRow(s.toObjects());
				}
			} catch (dangSoEX ex) {
				JOptionPane.showMessageDialog(null, "so luong la gia tri nguyen duong");
			}
		});
		// sử lí nút bấm thanh toán trong tính công
		thanhtoanBTN.addActionListener((e) -> {
			int ma = Integer.valueOf(maphongvienTCCB.getItemAt(maphongvienTCCB.getSelectedIndex()));
			long tong = 0, s = 0;
			DefaultTableModelTC.setRowCount(0);
			for (DanhSach a : listDs) {
				if (a.getA().getMaPV() == ma) {
					DefaultTableModelTC.addRow(a.toObjects());
					tong += a.getThanhtien();
					s = 1;
				}

			}
			if (s == 0)
				JOptionPane.showMessageDialog(null, "khong co bai viet nao");
			tongluongTF.setText(tong + "");
		});
		// sử lí nút bấm sắp xếp trong danh sách
		sortDSBTN.addActionListener((e) -> {
			String s = sortCB.getItemAt(sortCB.getSelectedIndex());
			if (s.equals("So luong bai")) {
				Collections.sort(listDs, (o1, o2) -> {
					return o1.getSl() > o2.getSl() ? -1 : 1;
				});
			} else {
				Collections.sort(listDs, (o1, o2) -> {
					return o1.getA().getTen().compareTo(o2.getA().getTen());
				});
			}
			DefaultTableModelDS.setRowCount(0);
			for (DanhSach a : listDs) {
				DefaultTableModelDS.addRow(a.toObjects());
			}
		});
		// sử lí nút lưu trong Phóng Viên
		luuPVBTN.addActionListener((e) -> {
			luufile(listPv, FileFV);
			toast("Lưu PV thành công.");
		});
		// sử lí nút lưu trong kiểu bài viết
		luuKBVBTN.addActionListener((e) -> {
			luufile(listBv, FileKB);
			toast("Lưu KBV thành công.");
		});
		// sử lí nút lưu trong Danh sach
		luuDSBTN.addActionListener((e) -> {
			luufile(listDs, FileDS);
			toast("Lưu DSBTN thành công.");
		});
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed" desc="Generated
	// Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		jTabbedPane1 = new javax.swing.JTabbedPane();
		jPanel1 = new javax.swing.JPanel();
		jLabel1 = new javax.swing.JLabel();
		hotenPVTF = new javax.swing.JTextField();
		diachiPVTF = new javax.swing.JTextField();
		jLabel2 = new javax.swing.JLabel();
		mucdoPVCB = new javax.swing.JComboBox<>();
		jScrollPane1 = new javax.swing.JScrollPane();
		tablePV = new javax.swing.JTable();
		themPVBTN = new javax.swing.JButton();
		luuPVBTN = new javax.swing.JButton();
		jLabel3 = new javax.swing.JLabel();
		jPanel2 = new javax.swing.JPanel();
		jLabel4 = new javax.swing.JLabel();
		tenKBVTF = new javax.swing.JTextField();
		jLabel5 = new javax.swing.JLabel();
		dongiaKBVTF = new javax.swing.JTextField();
		themKBVBTN = new javax.swing.JButton();
		luuKBVBTN = new javax.swing.JButton();
		jScrollPane2 = new javax.swing.JScrollPane();
		tableKBV = new javax.swing.JTable();
		jPanel3 = new javax.swing.JPanel();
		jLabel6 = new javax.swing.JLabel();
		soluongDSTF = new javax.swing.JTextField();
		themDSBTN = new javax.swing.JButton();
		luuDSBTN = new javax.swing.JButton();
		jScrollPane3 = new javax.swing.JScrollPane();
		tableDS = new javax.swing.JTable();
		jLabel7 = new javax.swing.JLabel();
		maphongvienDSCB = new javax.swing.JComboBox<>();
		jLabel8 = new javax.swing.JLabel();
		mabaivietDSCB = new javax.swing.JComboBox<>();
		sortDSBTN = new javax.swing.JButton();
		sortCB = new javax.swing.JComboBox<>();
		jPanel4 = new javax.swing.JPanel();
		jLabel9 = new javax.swing.JLabel();
		maphongvienTCCB = new javax.swing.JComboBox<>();
		thanhtoanBTN = new javax.swing.JButton();
		jScrollPane4 = new javax.swing.JScrollPane();
		tableTc = new javax.swing.JTable();
		jLabel11 = new javax.swing.JLabel();
		tongluongTF = new javax.swing.JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		jLabel1.setText("Họ tên");

		jLabel2.setText("Địa chỉ");

		mucdoPVCB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "chuyên nghiệp", "nghiệp dư" }));

		tablePV.setModel(new javax.swing.table.DefaultTableModel(new Object[][] {

		}, new String[] { "Mã ", "Họ tên", "Địa chỉ", "Mức độ" }));
		jScrollPane1.setViewportView(tablePV);

		themPVBTN.setText("Thêm");

		luuPVBTN.setText("Lưu");

		jLabel3.setText("Mức độ");

		JButton deleteButton = new JButton("Xóa");
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = tablePV.getSelectedRow();

				if (index == -1) {
					toast("Bạn chưa chọn PV nào.");
					return;
				}
				CustomDialog customDialog = new CustomDialog(
						"Bạn có chắc chắn muốn xóa nhận viên " + listPv.get(index).getTen() + " ?",
						new ActionCustomDialog() {

							@Override
							public void onConfirmed() {
								listPv.remove(index);
								luufile(listPv, FileFV);
								toast("Xóa PV thành công.");
								DefaultTableModelPV.removeRow(index);
							}

						});
				customDialog.show();

			}
		});

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING).addGroup(jPanel1Layout
				.createSequentialGroup()
				.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout
								.createParallelGroup(Alignment.LEADING, false)
								.addComponent(jLabel1, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
								.addComponent(jLabel2, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
								.addComponent(mucdoPVCB, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
								.addComponent(diachiPVTF, GroupLayout.DEFAULT_SIZE, 182, Short.MAX_VALUE)
								.addComponent(hotenPVTF)))
						.addGroup(jPanel1Layout.createSequentialGroup().addGap(40)
								.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
										.addComponent(luuPVBTN, GroupLayout.PREFERRED_SIZE, 75,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(themPVBTN, GroupLayout.PREFERRED_SIZE, 75,
												GroupLayout.PREFERRED_SIZE)))
						.addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addComponent(jLabel3))
						.addGroup(jPanel1Layout.createSequentialGroup().addGap(52).addComponent(deleteButton)))
				.addGap(28).addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 654, Short.MAX_VALUE)));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup().addGap(41).addComponent(jLabel1)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(hotenPVTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addGap(38).addComponent(jLabel2).addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(diachiPVTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addGap(26).addComponent(jLabel3).addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(mucdoPVCB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addGap(47).addComponent(themPVBTN).addGap(18).addComponent(luuPVBTN).addGap(18)
										.addComponent(deleteButton))
								.addGroup(jPanel1Layout.createSequentialGroup().addGap(22).addComponent(jScrollPane1,
										GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(53, Short.MAX_VALUE)));
		jPanel1Layout.linkSize(SwingConstants.VERTICAL, new Component[] { themPVBTN, luuPVBTN, deleteButton });
		jPanel1.setLayout(jPanel1Layout);

		jTabbedPane1.addTab("Phóng Viên", jPanel1);

		jLabel4.setText("Tên bài viết");

		jLabel5.setText("Đơn giá");

		dongiaKBVTF.setText("0");

		themKBVBTN.setText("Thêm");

		luuKBVBTN.setText("Lưu");

		tableKBV.setModel(new javax.swing.table.DefaultTableModel(new Object[][] {

		}, new String[] { "Mã", "Tên Bài", "Giá" }));
		jScrollPane2.setViewportView(tableKBV);

		deletePost = new JButton("Xóa");
		deletePost.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = tableKBV.getSelectedRow();
				if (index == -1) {
					toast("Bạn chưa chọn KBV nào.");
					return;
				}
				CustomDialog customDialog = new CustomDialog(
						"Bạn có chắc chắn muốn xóa bài viết " + listBv.get(index).getMa() + " ?",
						new ActionCustomDialog() {

							@Override
							public void onConfirmed() {
								listBv.remove(index);
								luufile(listBv, FileKB);
								toast("Xóa BV thành công.");
								DefaultTableModelBV.removeRow(index);
							}

						});
				customDialog.show();
			
			
			}
		});

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
		jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING).addGroup(jPanel2Layout
				.createSequentialGroup()
				.addGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING).addGroup(jPanel2Layout
						.createSequentialGroup().addContainerGap()
						.addGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING).addGroup(jPanel2Layout
								.createParallelGroup(Alignment.LEADING, false)
								.addComponent(jLabel4, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
								.addComponent(jLabel5, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
								.addComponent(dongiaKBVTF)
								.addComponent(tenKBVTF, GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE))
								.addGroup(jPanel2Layout.createSequentialGroup().addGap(30)
										.addGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING)
												.addComponent(luuKBVBTN, GroupLayout.PREFERRED_SIZE, 75,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(themKBVBTN, GroupLayout.PREFERRED_SIZE, 75,
														GroupLayout.PREFERRED_SIZE)))))
						.addGroup(jPanel2Layout.createSequentialGroup().addGap(52).addComponent(deletePost)))
				.addGap(28).addComponent(jScrollPane2, GroupLayout.DEFAULT_SIZE, 644, Short.MAX_VALUE)
				.addContainerGap()));
		jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup().addGap(25)
						.addGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING)
								.addGroup(jPanel2Layout.createSequentialGroup().addGap(19).addComponent(jLabel4)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(tenKBVTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addGap(38).addComponent(jLabel5).addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(dongiaKBVTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addGap(63).addComponent(themKBVBTN).addGap(30).addComponent(luuKBVBTN)
										.addGap(18).addComponent(deletePost))
								.addComponent(jScrollPane2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addContainerGap(50, Short.MAX_VALUE)));
		jPanel2.setLayout(jPanel2Layout);

		jTabbedPane1.addTab("Kiểu bài viết", jPanel2);

		jLabel6.setText("Số lượng bài");

		soluongDSTF.setText("0");

		themDSBTN.setText("Thêm");

		luuDSBTN.setText("Lưu");

		tableDS.setModel(new javax.swing.table.DefaultTableModel(new Object[][] {

		}, new String[] { "Mã phong viên", "Tên phóng viên", "mã bài viết", "tên bài viết", "số lượng" }));
		jScrollPane3.setViewportView(tableDS);

		jLabel7.setText("Mã phóng viên");

		maphongvienDSCB.setModel(
				new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
		maphongvienDSCB.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				maphongvienDSCBActionPerformed(evt);
			}
		});

		jLabel8.setText("Mã bài viết");

		mabaivietDSCB.setModel(
				new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

		sortDSBTN.setText("Sắp xếp");

		sortCB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "So luong bai", "ten phong vien" }));

		deleteListBtn = new JButton("Xóa");
		deleteListBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = tableDS.getSelectedRow();
				if (index == -1) {
					toast("Bạn chưa chọn mục nào.");
					return;
				}
				CustomDialog customDialog = new CustomDialog(
						"Bạn có chắc chắn muốn xóa mục " + listDs.get(index).getA().getTen() + " - " + listDs.get(index).getB().getTenbai() + " ?",
						new ActionCustomDialog() {

							@Override
							public void onConfirmed() {
								listDs.remove(index);
								luufile(listDs, FileDS);
								toast("Xóa thành công.");
								DefaultTableModelDS.removeRow(index);
							}

						});
				customDialog.show();

			
			}
		});

		javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
		jPanel3Layout.setHorizontalGroup(jPanel3Layout.createParallelGroup(Alignment.LEADING).addGroup(jPanel3Layout
				.createSequentialGroup()
				.addGroup(jPanel3Layout.createParallelGroup(Alignment.LEADING).addGroup(jPanel3Layout
						.createParallelGroup(Alignment.LEADING, false)
						.addComponent(soluongDSTF, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
						.addComponent(jLabel6, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
						.addComponent(jLabel7).addComponent(jLabel8)
						.addComponent(mabaivietDSCB, 0, 136, Short.MAX_VALUE)
						.addComponent(maphongvienDSCB, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(jPanel3Layout.createSequentialGroup().addGap(20)
								.addGroup(jPanel3Layout.createParallelGroup(Alignment.TRAILING)
										.addComponent(luuDSBTN, GroupLayout.PREFERRED_SIZE, 75,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(themDSBTN, GroupLayout.PREFERRED_SIZE, 75,
												GroupLayout.PREFERRED_SIZE)))
						.addGroup(jPanel3Layout.createSequentialGroup().addContainerGap().addComponent(sortDSBTN,
								GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
						.addComponent(sortCB, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addGroup(jPanel3Layout.createSequentialGroup().addGap(32).addComponent(deleteListBtn)))
				.addPreferredGap(ComponentPlacement.RELATED)
				.addComponent(jScrollPane3, GroupLayout.PREFERRED_SIZE, 713, GroupLayout.PREFERRED_SIZE)
				.addContainerGap(19, Short.MAX_VALUE)));
		jPanel3Layout
				.setVerticalGroup(
						jPanel3Layout.createParallelGroup(Alignment.TRAILING)
								.addGroup(jPanel3Layout.createSequentialGroup().addGap(68).addComponent(jLabel7)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(maphongvienDSCB, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGap(18).addComponent(jLabel8).addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(mabaivietDSCB, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGap(22).addComponent(jLabel6).addGap(18)
										.addComponent(soluongDSTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addGap(20).addComponent(themDSBTN).addGap(18).addComponent(luuDSBTN).addGap(18)
										.addComponent(deleteListBtn)
										.addPreferredGap(ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
										.addComponent(sortCB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addGap(18).addComponent(sortDSBTN).addGap(39))
								.addGroup(
										jPanel3Layout.createSequentialGroup().addContainerGap(44, Short.MAX_VALUE)
												.addComponent(jScrollPane3, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addGap(31)));
		jPanel3.setLayout(jPanel3Layout);

		jTabbedPane1.addTab("Danh sách ", jPanel3);

		jLabel9.setText("Mã phóng viên");

		maphongvienTCCB.setModel(
				new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
		maphongvienTCCB.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				maphongvienTCCBActionPerformed(evt);
			}
		});

		thanhtoanBTN.setText("Thanh toan");

		tableTc.setModel(new javax.swing.table.DefaultTableModel(new Object[][] {

		}, new String[] { "Mã phong viên", "Tên phóng viên", "mã bài viết", "tên bài viết", "số lượng", "gia",
				"thanh tien" }));
		jScrollPane4.setViewportView(tableTc);

		jLabel11.setText("Tổng lương :");

		tongluongTF.setText("00");

		javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
		jPanel4.setLayout(jPanel4Layout);
		jPanel4Layout.setHorizontalGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
						jPanel4Layout.createSequentialGroup()
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 83,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGap(18, 18, 18)
								.addComponent(tongluongTF, javax.swing.GroupLayout.PREFERRED_SIZE, 153,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGap(30, 30, 30))
				.addGroup(jPanel4Layout.createSequentialGroup().addContainerGap().addComponent(thanhtoanBTN)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
						.addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 700,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
				.addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(jPanel4Layout.createSequentialGroup().addGap(9, 9, 9)
								.addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(jLabel9)
										.addComponent(maphongvienTCCB, javax.swing.GroupLayout.PREFERRED_SIZE, 136,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addContainerGap(729, Short.MAX_VALUE))));
		jPanel4Layout
				.setVerticalGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
								.addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(jPanel4Layout.createSequentialGroup().addGap(140, 140, 140)
												.addComponent(thanhtoanBTN)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
														javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout
												.createSequentialGroup().addContainerGap(36, Short.MAX_VALUE)
												.addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 383,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addGap(28, 28, 28)))
								.addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(tongluongTF, javax.swing.GroupLayout.PREFERRED_SIZE, 14,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(jLabel11))
								.addGap(37, 37, 37))
						.addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(jPanel4Layout.createSequentialGroup().addGap(66, 66, 66).addComponent(jLabel9)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(maphongvienTCCB, javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))));

		jTabbedPane1.addTab("Tính công", jPanel4);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				javax.swing.GroupLayout.Alignment.TRAILING,
				layout.createSequentialGroup().addContainerGap().addComponent(jTabbedPane1).addContainerGap()));
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jTabbedPane1));

		pack();
	}// </editor-fold>//GEN-END:initComponents

	private void maphongvienDSCBActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_maphongvienDSCBActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_maphongvienDSCBActionPerformed

	private void maphongvienTCCBActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_maphongvienTCCBActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_maphongvienTCCBActionPerformed

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting code
		// (optional) ">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the default
		 * look and feel. For details see
		 * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(view.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(view.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(view.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(view.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new view().setVisible(true);
			}
		});
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JTextField diachiPVTF;
	private javax.swing.JTextField dongiaKBVTF;
	private javax.swing.JTextField hotenPVTF;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel11;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JLabel jLabel7;
	private javax.swing.JLabel jLabel8;
	private javax.swing.JLabel jLabel9;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JPanel jPanel3;
	private javax.swing.JPanel jPanel4;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JScrollPane jScrollPane3;
	private javax.swing.JScrollPane jScrollPane4;
	private javax.swing.JTabbedPane jTabbedPane1;
	private javax.swing.JButton luuDSBTN;
	private javax.swing.JButton luuKBVBTN;
	private javax.swing.JButton luuPVBTN;
	private javax.swing.JComboBox<String> mabaivietDSCB;
	private javax.swing.JComboBox<String> maphongvienDSCB;
	private javax.swing.JComboBox<String> maphongvienTCCB;
	private javax.swing.JComboBox<String> mucdoPVCB;
	private javax.swing.JTextField soluongDSTF;
	private javax.swing.JComboBox<String> sortCB;
	private javax.swing.JButton sortDSBTN;
	private javax.swing.JTable tableDS;
	private javax.swing.JTable tableKBV;
	private javax.swing.JTable tablePV;
	private javax.swing.JTable tableTc;
	private javax.swing.JTextField tenKBVTF;
	private javax.swing.JButton thanhtoanBTN;
	private javax.swing.JButton themDSBTN;
	private javax.swing.JButton themKBVBTN;
	private javax.swing.JButton themPVBTN;
	private javax.swing.JLabel tongluongTF;
	private JButton deletePost;
	private JButton deleteListBtn;
	// End of variables declaration//GEN-END:variables

	private void luufile(ArrayList a, String file) {
		IoFile.ghiFile(a, file);

	}

	private void toast(String msg) {
		JOptionPane.showMessageDialog(null, msg);
	}

	private void docfilevaotabel() {
		// doc du lieu từ file qua list
		IoFile.docFile(listPv, FileFV);
		IoFile.docFile(listBv, FileKB);
		IoFile.docFile(listDs, FileDS);

		// ép lần lượt vào table
		for (PhongVien pv : listPv) {
			DefaultTableModelPV.addRow(pv.toObjects());
		}
		for (BaiViet kb : listBv) {
			DefaultTableModelBV.addRow(kb.toObjects());
		}
		for (DanhSach pv : listDs) {
			DefaultTableModelDS.addRow(pv.toObjects());
		}
	}

	// lấy các lựa chọn mã của phóng viên và bài viết vào combo box
	private void napComb() {
		maphongvienDSCB.removeAllItems();
		mabaivietDSCB.removeAllItems();
		maphongvienTCCB.removeAllItems();
		for (PhongVien pv : listPv) {
			maphongvienDSCB.addItem(pv.getMaPV() + "");
			maphongvienTCCB.addItem(pv.getMaPV() + "");
		}
		for (BaiViet pv : listBv) {
			mabaivietDSCB.addItem(pv.getMa() + "");
		}
	}
}
