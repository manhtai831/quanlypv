package view;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

public class CustomDialog {
	private JFrame frame = new JFrame();
	private String msg;
	private ActionCustomDialog action;

	public CustomDialog(String msg, ActionCustomDialog action) {
		this.msg = msg;
		this.action = action;
		frame.setTitle("Thông báo");
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(10, 10, 10, 10));

		JLabel displayText = new JLabel(msg);
		panel.add(displayText);
		displayText.setBorder(new EmptyBorder(0, 0, 16, 0));
		JButton btn = new JButton("OK");
		JButton btnCancel = new JButton("Hủy");

		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				action.onConfirmed();
			}
		});

		btnCancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
			}
		});

		panel.add(btn);

		panel.add(btnCancel);
		JPanel panelButton = new JPanel();
		panelButton.add(btn);
		panelButton.add(btnCancel);
panel.add(panelButton);
		
		frame.add(panel);
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);

		frame.setLocation(200, 200);
		frame.setSize(350, 150);

	}

	void show() {
		frame.setVisible(true);
	}
}
