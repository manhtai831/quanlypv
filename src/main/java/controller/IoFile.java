/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lehuy
 */
public class IoFile {
    //ghi dữ liệu từ list vào file
    public static void ghiFile(ArrayList a, String file){
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));){
            for (Object tmp : a) {
                oos.writeObject(tmp);
            }
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
    }
    // doc du lieu tu file vào list
    public static void docFile(ArrayList a, String file){
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));) {
            
            Object tmp;
            while((tmp=ois.readObject())!=null){
                a.add(tmp);
            }
  
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } catch (ClassNotFoundException ex) {
        }
    }
}
