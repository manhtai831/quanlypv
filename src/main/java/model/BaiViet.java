/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import controller.*;
import java.io.Serializable;
import java.util.ArrayList;
/**
 *
 * @author lehuy
 */
public class BaiViet implements Serializable{
    private int ma, gia;
    private String tenbai;

    public BaiViet(int i,  String tenbai, int gia)throws dangSoEX, dangChuEX, deTrongEX{
        if(tenbai.equals("")) throw new deTrongEX(); // sinh ra ngoại lệ không đc bỏ trông
        if(gia<=0) throw new dangSoEX(); // sinh ra ngoại lệ phaior là dạng sô
        this.ma = 101+i;
        this.gia = gia;
        this.tenbai = chuanHoa(tenbai);
    }
    private String chuanHoa(String ten){
        ten = ten.trim();
        String []ds= ten.toLowerCase().split("\\s+");
        String tmp="";
        for (String d : ds) {
            tmp+= d.substring(0, 1).toUpperCase()+ d.substring(1)+" ";
        }
        return tmp.substring(0, tmp.length()-1).toString();
    }
    public int getMa() {
        return ma;
    }

    public int getGia() {
        return gia;
    }

    public String getTenbai() {
        return tenbai;
    }
    //ktra xem có tông tại bài viết chưa
    public boolean trungnhau(ArrayList<BaiViet> k){
        for (BaiViet bv : k) {
            if(this.tenbai.equals(bv.tenbai))
                return true;
        }
        return false;
    }
    public Object[] toObjects(){
        return new Object[]{ma, tenbai, gia};
    }
}
