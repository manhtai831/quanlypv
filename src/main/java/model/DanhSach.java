/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import controller.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author lehuy
 */
public class DanhSach implements Serializable{
    private PhongVien a;
    private BaiViet b;
    private int sl, gia;
    private long thanhtien;

    public DanhSach(PhongVien a, BaiViet b, int sl) throws dangSoEX{
        if(sl<=0) throw new dangSoEX(); // sinh ra ngoại lệ dạng số
        this.a = a;
        this.b = b;
        this.sl = sl;
        this.gia=1;
        this.thanhtien = 1;
    }

    public void setThanhtien() {
        this.thanhtien = sl*gia;
    }

    public void setGia(int gia) {
        this.gia = gia;
    }
    
    public boolean trungnhau(ArrayList<DanhSach> k){
        for (DanhSach ds : k) {
            if(this.a.getMaPV() == ds.a.getMaPV() && this.b.getMa()==ds.b.getMa())
                return true;
        }
        return false;
    }
    
    public Object[] toObjects(){
        return new Object[]{a.getMaPV(), a.getTen(), b.getMa(), b.getTenbai(), sl, gia,thanhtien};
    }

    public PhongVien getA() {
        return a;
    }

    public long getThanhtien() {
        return thanhtien;
    }

    public int getSl() {
        return sl;
    }

	public BaiViet getB() {
		return b;
	}

	public void setB(BaiViet b) {
		this.b = b;
	}

    
}
