/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import java.util.*;
import controller.*;
import java.io.Serializable;
/**
 *
 * @author lehuy
 */
public class PhongVien implements Serializable{
    private String ten, diachi, mucdo;
    private int maPV;

    public PhongVien(int i, String tenPV, String diachiPV, String mucdoPV) throws deTrongEX,dangChuEX{
        if(tenPV.equals("") || diachiPV.equals("")) throw new deTrongEX(); // sinh ra ngoại lệ không đc bỏ trông
        if(kiemTra.chuCai(tenPV)) throw new dangChuEX();// sinh ra ngoại lệ khobng phải dạng chưc cái
        this.ten = chuanHoa(tenPV);
        this.diachi= chuanHoa(diachiPV);
        this.mucdo = mucdoPV;
        this.maPV = 1001+i;
    }
    // chuan hoa lại họ tên
    private String chuanHoa(String ten){
        ten = ten.trim();
        String []ds= ten.toLowerCase().split("\\s+");
        String tmp="";
        for (String d : ds) {
            tmp+= d.substring(0, 1).toUpperCase()+ d.substring(1)+" ";
        }
        return tmp.substring(0, tmp.length()-1).toString();
    }
    public String getTen() {
        return ten;
    }

    public String getDiachi() {
        return diachi;
    }

    public String getMucdo() {
        return mucdo;
    }

    public int getMaPV() {
        return maPV;
    }
    //ktra xem đã có Phóng viên này chưa
    public boolean trungnhau(ArrayList<PhongVien> k){
        for (PhongVien bv : k) {
            if(this.ten.equals(bv.ten) && this.diachi.equals(bv.diachi)&& this.mucdo.equals(bv.mucdo) )
                return true;
        }
        return false;
    }
    public Object[] toObjects(){
        return new Object[]{maPV, ten, diachi,mucdo};
    }
}
